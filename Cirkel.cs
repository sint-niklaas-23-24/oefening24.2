﻿namespace Oefening_23._2___Bart
{
    internal class Cirkel
    {
        private double _straal;

        public Cirkel() { }
        public Cirkel(double x)
        {
            Straal = x;
        }
        public double Straal { get; set; }

        public double BerekenOmtrek()
        {
            double omtrek = Math.Round((2 * Straal * Math.PI), 2);
            return omtrek;
        }
        public double BerekenOppervlakte()
        {
            return Math.Round((Math.Pow(Straal, 2) * Math.PI), 2);
        }
        public string FormattedOmtrek()
        {
            return BerekenOmtrek().ToString("0.00");
        }
        public string FormattedOppervlakte()
        {
            return BerekenOppervlakte().ToString("0.00");
        }
        public override string ToString()
        {
            return "Omtrek: " + FormattedOmtrek().PadLeft(10) + Environment.NewLine + "Oppervlakte: "
                    + FormattedOppervlakte().PadLeft(5) + Environment.NewLine;
        }
    }
}
